package com.sa.sa_section2;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

public class ContactListAdapter extends RecyclerView.Adapter<ContactListAdapter.MyViewHolder> {

    private LayoutInflater mLayoutInflater;
    private Context ctx;
    private ArrayList<Contact> contacts;
    private int  mViewResourceId;


    public static class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView contactNumber;

        public MyViewHolder(View v) {
            super(v);
            contactNumber = (TextView)itemView.findViewById(R.id.tvContactName);

        }

    }

    public ContactListAdapter(Context ctx, ArrayList<Contact> contacts, int tvResourceId){
        this.ctx = ctx;
        this.contacts = contacts;
        mLayoutInflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mViewResourceId = tvResourceId;
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {


        View view = mLayoutInflater.inflate(R.layout.contact_adapter_view, parent, false);
        MyViewHolder holder = new MyViewHolder(view);
        return holder;
    }





    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        Contact contact = contacts.get(position);
        if (holder != null) {

            if (holder.contactNumber != null) {
                holder.contactNumber.setText(contact.getPhoneNumber());
            }
        }
    }

    @Override
    public int getItemCount() {
        return contacts.size();
    }


}



