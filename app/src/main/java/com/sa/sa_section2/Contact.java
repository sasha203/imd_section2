package com.sa.sa_section2;

public class Contact {

    private String imsiNumber;
    private String phoneNumber;
    private boolean verified = false;

    public Contact() {}


    public Contact (String imsiNumber, String phoneNumber) {
        this.imsiNumber = imsiNumber;
        this.phoneNumber = phoneNumber;
    }


    public boolean isVerified() {
        return verified;
    }

    public void setToVerified() {
        this.verified = true;
    }


    public String getImsiNumber() {
        return imsiNumber;
    }

    public String getPhoneNumber() {
       return phoneNumber;
    }

    public void setImsi(String imsiNumber) {
        this.imsiNumber = imsiNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }





}
