package com.sa.sa_section2;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.telephony.SmsManager;
import android.telephony.SmsMessage;
import android.util.Log;
import android.widget.Toast;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class SmsBroadcastReceiver extends BroadcastReceiver {

    Context ctx;
    FirebaseDatabase database = FirebaseDatabase.getInstance();
    DatabaseReference ref = database.getReference("Contacts");
    String targetPhoneNumber;


    public SmsBroadcastReceiver(String targetPhoneNumber ,Context ctx) {
        this.targetPhoneNumber = targetPhoneNumber;
        this.ctx = ctx;
    }



    @Override
    public void onReceive(Context context, Intent intent) {

        //Get the data (SMS data) bound to intent
        Bundle bundle;
        //SmsMessage[] msgs = null;
        //String str = "";





        if (intent.getAction().equals("android.provider.Telephony.SMS_RECEIVED")) {
            bundle = intent.getExtras();



            ref.orderByChild("phoneNumber").equalTo(targetPhoneNumber).limitToFirst(1).addChildEventListener(new ChildEventListener() {
                @Override
                public void onChildAdded(DataSnapshot dataSnapshot, String prevChildKey) {

                    Contact targetContact; //the Contact who you send the message. (Getting the data from this contact)

                    if(dataSnapshot.hasChild("phoneNumber")) {

                        //Toast.makeText(ctx, "" + dataSnapshot.getChildren(), Toast.LENGTH_SHORT).show();
                        targetContact = dataSnapshot.getValue(Contact.class); //populate with data.

                        if(!targetContact.isVerified()) {

                            Log.d("DEEDEE", "numb:  " + targetContact.getPhoneNumber() + "\nimsi: " + targetContact.getImsiNumber());

                            //set verify to true
                            targetContact.setToVerified();
                            //db Update on verifyied
                           //ref.updateChildren(targetContact);
                            Log.d("DEEDEE", "Verifyed: " + targetContact.isVerified());
                            Toast.makeText(ctx, "aaa" + targetContact.isVerified(), Toast.LENGTH_SHORT).show();
                        }
                    }
                }

                @Override
                public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

                }

                @Override
                public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

                }

                @Override
                public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }


            });


            /*
            //if there is a msg
            if (bundle != null) {
                //Retrieve the sms received.
                //pdus = (Packet Data Unit)
                Object[] pdus = (Object[]) bundle.get("puds");

                msgs = new SmsMessage[pdus.length];
                if (pdus != null) {
                    //Forevery msg received
                    for (int i = 0; i < msgs.length; i++) {
                        msgs[i] = SmsMessage.createFromPdu((byte[]) pdus[i], "");
                        str += "SMS from " + msgs[i].getOriginatingAddress() + " : ";
                        str += msgs[i].getMessageBody().toString();
                        str += "\n";
                    }

                    Toast.makeText(context, str, Toast.LENGTH_SHORT).show();
                }
            }*/
        }

    }
}
