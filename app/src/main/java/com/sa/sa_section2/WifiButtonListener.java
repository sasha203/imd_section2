package com.sa.sa_section2;

import android.content.Context;
import android.net.wifi.WifiManager;
import android.util.Log;
import android.view.View;

public class WifiButtonListener implements View.OnClickListener {
    Context ctx;

    WifiButtonListener(Context ctx) {
        this.ctx = ctx;
    }

    @Override
    public void onClick(View v) {
        WifiManager wifi = (WifiManager) ctx.getSystemService(Context.WIFI_SERVICE);
        wifi.setWifiEnabled(true);
    }
}
