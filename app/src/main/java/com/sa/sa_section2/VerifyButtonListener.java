package com.sa.sa_section2;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.provider.Telephony;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.telephony.SmsManager;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;


import java.lang.annotation.Target;

import static com.google.firebase.database.FirebaseDatabase.getInstance;


public class VerifyButtonListener implements View.OnClickListener {
    Context ctx;
    private final int REQUEST_SMS;
    private final int REQUEST_PHONE_STATE;

    FirebaseDatabase database = FirebaseDatabase.getInstance();
    DatabaseReference  ref = database.getReference("Contacts");



    TelephonyManager tm;

    EditText etNum;
    String targetPhoneNumber;

    //String myPhoneNumb;
    String myImsi;



    VerifyButtonListener(Context ctx, int REQUEST_SMS, int REQUEST_PHONE_STATE,  EditText etNum) {
        this.ctx = ctx;
        this.REQUEST_SMS = REQUEST_SMS;
        this.REQUEST_PHONE_STATE = REQUEST_PHONE_STATE;
        this.etNum = etNum;
    }


    @Override
    public void onClick(View v) {
        //Log.d("CLICK", "Verify clicked");
        if (!isSmsPermissionGranted()) {
            requestReadAndSendSmsPermission();
        } else {

            ctx.registerReceiver(new SmsBroadcastReceiver(targetPhoneNumber, ctx), new IntentFilter("android.provider.Telephony.SMS_RECEIVED"));

            tm = (TelephonyManager) ctx.getSystemService(Context.TELEPHONY_SERVICE);

            if (ActivityCompat.checkSelfPermission(ctx, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                ps_AlertDialog();
                return;
            }


            myImsi = tm.getSubscriberId(); //imea - tm.getDeviceId();
            //myImsi = tm.getDeviceId();
            //myPhoneNumb = tm.getLine1Number();

            //will store inputted phone number.
            targetPhoneNumber = etNum.getText().toString();

            if(targetPhoneNumber.length() < 4) { //did it 4 due to port numbers on emulators.

                Toast.makeText(ctx, "Please Entered a valid number.", Toast.LENGTH_SHORT).show();
            }
            else {

                String id = ref.push().getKey(); //Generate id. (for Db)

                if (myImsi != null) {

                    final Contact newContact = new Contact(myImsi, targetPhoneNumber); //"_*" +
                    ref.child(id).setValue(newContact);
                   // Toast.makeText(ctx, "Contact Added", Toast.LENGTH_SHORT).show();

                    //send verification code to number entered.
                    SmsManager.getDefault().sendTextMessage(targetPhoneNumber, null, "Verification code: " + myImsi, null, null);


                }
            }

        }
    }


    //SMS Permission methods.-----------------------------------------------------------------------

    public boolean isSmsPermissionGranted() {
        boolean result = ActivityCompat.checkSelfPermission(ctx, Manifest.permission.READ_SMS) == PackageManager.PERMISSION_GRANTED;
        Log.d("PERM STATUS" , "permission - " + result);
        return result;
    }

    //will call info and then actuall permission query.
    private void requestReadAndSendSmsPermission() {
        showRequestPermissionsInfoAlertDialog(true);
    }



    public void showRequestPermissionsInfoAlertDialog(final boolean makeSystemRequest) {
        AlertDialog.Builder builder = new AlertDialog.Builder(ctx);
        builder.setTitle(R.string.permission_alert_dialog_title);
        builder.setMessage(R.string.permission_dialog_msg);

        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                // Display system runtime permission request?
                if (makeSystemRequest) {
                    //request permission
                    ActivityCompat.requestPermissions((Activity)ctx, new String[]{Manifest.permission.READ_SMS}, REQUEST_SMS);

                }
            }
        });

        builder.setCancelable(false);
        builder.show();
    }


    //----------------------------------------------------------------------------------------------




    //Phone Permissions
    //----------------------------------------------------------------------------------------------
    public void ps_AlertDialog() {

        ActivityCompat.requestPermissions((Activity)ctx, new String[]{Manifest.permission.READ_PHONE_STATE},
                REQUEST_PHONE_STATE);

        AlertDialog.Builder builder = new AlertDialog.Builder(ctx);
        builder.setTitle(R.string.ps_permission_alert_dialog_title);
        builder.setMessage(R.string.ps_permission_dialog_msg);

        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                ActivityCompat.requestPermissions((Activity)ctx, new String[]{Manifest.permission.READ_PHONE_STATE}, REQUEST_PHONE_STATE);
            }
        });

        builder.setCancelable(false);
        builder.show();
    }
    //----------------------------------------------------------------------------------------------






}
