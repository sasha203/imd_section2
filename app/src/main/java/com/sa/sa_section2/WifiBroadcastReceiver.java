package com.sa.sa_section2;

import android.app.Activity;
import android.app.DownloadManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;

public class WifiBroadcastReceiver extends BroadcastReceiver {


    Context ctx;
    Button btnVerify;
    EditText etNum;
    final int REQUEST_SMS;


    public WifiBroadcastReceiver(Context ctx, Button btnVerify, EditText etNum, final int REQUEST_SMS ) {
        this.ctx = ctx;
        this.btnVerify = btnVerify;
        this.etNum = etNum;
        this.REQUEST_SMS = REQUEST_SMS;
    }

    @Override
    public void onReceive(Context context, Intent intent) {

        NetworkInfo info = intent.getParcelableExtra(WifiManager.EXTRA_NETWORK_INFO);

        if(info != null && info.isConnected()) {
            //((Activity)context).setContentView(R.layout.activity_main); //di bdiet terga tireloadja l activity mil gdid.

            ctx.unregisterReceiver(this);
            btnVerify.setEnabled(true);
            etNum.setEnabled(true);
        }

    }
}
