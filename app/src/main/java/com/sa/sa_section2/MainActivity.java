package com.sa.sa_section2;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.net.wifi.WifiManager;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.ArrayList;


public class MainActivity extends AppCompatActivity {

    Snackbar connectionSb = null;
    String connTest = "WAJFAJ";
    Button btnVerify;
    EditText etNum;

    public ContactListAdapter mContactListAdapter;
    public ArrayList<Contact> contacts;
    RecyclerView mRecyclerView;
    RecyclerView.LayoutManager mLayoutManager;


    private static final int REQUEST_WIFI = 0;
    private static final int REQUEST_SMS = 1;
    private static final int REQUEST_PHONE_STATE = 2;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnVerify = findViewById(R.id.btn_verify);
        etNum = findViewById(R.id.et_num);
        btnVerify.setOnClickListener(new VerifyButtonListener(this, REQUEST_SMS, REQUEST_PHONE_STATE, etNum));

        //Crushing the app.
        //registerReceiver(new SmsBroadcastReceiver(etNum.getText().toString(), this), new IntentFilter("android.provider.Telephony.SMS_RECEIVED"));

        contacts = new ArrayList<>();
        //recyclerView
        mRecyclerView = (RecyclerView) findViewById(R.id.rvContacts);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);


        //wifi permission
        if (!isWifiPermissionGranted()) {
            Log.d(connTest , "perm not granted");
            requestWifiPermission();
        }
        else {
            WifiManager wifi = (WifiManager) getApplicationContext().getSystemService(this.WIFI_SERVICE);

            if (!wifi.isWifiEnabled()) {
                connectSnackbar();
            }


        }


    } //onCreate end



    public void connectSnackbar() {
        Log.d(connTest, "MITFI");
        btnVerify.setEnabled(false);
        etNum.setEnabled(false);

        connectionSb = Snackbar.make(findViewById(R.id.activity_main),
                "Wi-Fi Disconnected", Snackbar.LENGTH_INDEFINITE);

        connectionSb.setAction("Connect", new WifiButtonListener(this));
        connectionSb.show();

        registerReceiver(new WifiBroadcastReceiver(this, btnVerify, etNum, REQUEST_SMS), new IntentFilter("android.net.wifi.STATE_CHANGE"));


    }


    //WiFi Permissions Methods (Currently using)
    public boolean isWifiPermissionGranted() {
        boolean result = ActivityCompat.checkSelfPermission(this,
                Manifest.permission.CHANGE_WIFI_STATE) == PackageManager.PERMISSION_GRANTED;
                Log.d(connTest , "Current Perm " + result);
        return result;
    }

    //dan li kont nuza l antik  (lijlu qed nuza xorta)
    private void requestWifiPermission() {
        Log.i("Perm", "Requesting Wi-Fi permission.");
        // Toast.makeText(this, "NOT GRANTED ", Toast.LENGTH_LONG).show();

        if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                Manifest.permission.CHANGE_WIFI_STATE)) {
            //Awek Mhux jithol meta namel dil if condition.
            Log.i("Perm", "Displaying WIFI permission rationale");
            Snackbar.make(findViewById(R.id.activity_main), R.string.permission_wifi_rationale,
                    Snackbar.LENGTH_INDEFINITE)
                    .setAction("Ok", new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            ActivityCompat.requestPermissions(MainActivity.this,
                                    new String[]{Manifest.permission.CHANGE_WIFI_STATE}, REQUEST_WIFI);
                        }
                    })
                    .show();
        }
        else {
            // permission has not been granted yet. Request it directly.
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CHANGE_WIFI_STATE},
                    REQUEST_WIFI);
        }

    }


    //Will handle all Permissions results.
    //Wifi case qied awn alxejn peress lid dialog mhux jidher Atm(Tal wifi).
    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            //0
            case REQUEST_WIFI : {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    Toast.makeText(getApplicationContext(), R.string.wifi_permissions_granted, Toast.LENGTH_SHORT).show();
                }
                else {
                    Toast.makeText(getApplicationContext(),  R.string.wifi_permissions_denied, Toast.LENGTH_SHORT).show();
                }
                return;

            }

            //1
            case REQUEST_SMS : {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    Toast.makeText(getApplicationContext(), R.string.sms_permission_granted, Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getApplicationContext(), R.string.sms_permission_denied, Toast.LENGTH_SHORT).show();
                }
                return;
            }


            //2
            case REQUEST_PHONE_STATE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    Toast.makeText(this, R.string.ps_permissions_granted, Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(this, R.string.ps_permissions_denied, Toast.LENGTH_SHORT).show();
                }
                return;

            }

        }
    }




    private BroadcastReceiver populateRecylcerView = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();
            Log.d("AF", "onReceive: ACTION FOUND.");

            //if Contact is Verified.....
            //



           mContactListAdapter = new ContactListAdapter(context ,contacts, R.layout.contact_adapter_view);
            mRecyclerView.setAdapter(mContactListAdapter);
        }
    };




}//end of class MainActivity
